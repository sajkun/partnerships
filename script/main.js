


var mobile_menu = function(target, _class_toggle, _class_off, _class_on ){

  this.humburger = jQuery(_class_toggle),

  this.menu = jQuery(target),

  this.init = function(){
    if(!_class_toggle || !target){
      return false;
    }
    var switcher = this.humburger;

    var menu = this.menu;

     jQuery(document).on('touchstart', _class_toggle, function(){
       switcher.toggleClass('active');
       menu.toggleClass('shown');
     })

     if(!_class_off){
      return false;
     }

     jQuery(document).on('touchstart', _class_off, function(){
       switcher.removeClass('active');
       menu.removeClass('shown');
     })
  }
}

function change_list(id, action){
  var list = jQuery(id);
  var val = parseInt(list.val());
  val = (action == 'increase')? val + 1 : val - 1;
  val = (val < 1)? 1 : val;
  val = (val > 5)? 5 : val;

  list.val(val);
  list.find('option').eq(val-1).prop({selected: 'selected'}).siblings('option').prop({selected: ''});

  recalc_onclick('pA4J');
}



function init_carousel(){
  if(!jQuery('.carousel').length){
    return false;
  }

  var owl = jQuery('.carousel__body');

  owl.owlCarousel({
    items: 1,
    auto: true,
    autoplayTimeout: 3000,
    loop: true,
  })

  owl.siblings('div.carousel-ctrl').find('.next').click(function(event) {
    owl.trigger('next.owl.carousel');
  });

  owl.siblings('div.carousel-ctrl').find('.prev').click(function(event) {
    owl.trigger('prev.owl.carousel');
  });
}
jQuery(document).on('click', function(e){
  if(!jQuery(e.target).closest('.js-toggle').length){
    jQuery(document).find('.js-toggle').removeClass('active').removeClass('shown')
  }
})

jQuery(document).on('touchend', '.row-expand', function(){
  jQuery('.row-expand').removeClass('class name')
  jQuery(this).toggleClass('shown');
  jQuery(this).closest('tr').siblings('tr').find('.row-expand').removeClass('shown');

  jQuery(this).siblings('.expand').slideToggle('300');
  console.log(jQuery(this).siblings('.expand'))
  jQuery(this).closest('tr').siblings('tr').find('.expand').slideUp('300');
})

jQuery(document).on('click', '.faq__item-switcher, .faq__item-question', function(){
  jQuery(this).closest('.faq__item').addClass('expanded').find('.faq__item-answer').slideDown(300);

  jQuery(this).closest('.faq__item').siblings('.faq__item').removeClass('expanded').find('.faq__item-answer').slideUp(300);
})
var Cookie =
{
   set: function(name, value, days)
   {
      var domain, domainParts, date, expires, host;

      if (days)
      {
         date = new Date();
         date.setTime(date.getTime()+(days*24*60*60*1000));
         expires = "; expires="+date.toGMTString();
      }
      else
      {
         expires = "";
      }

      host = location.host;
      if (host.split('.').length === 1)
      {
         // no "." in a domain - it's localhost or something similar
         document.cookie = name+"="+value+expires+"; path=/";
      }
      else
      {
         // Remember the cookie on all subdomains.
          //
         // Start with trying to set cookie to the top domain.
         // (example: if user is on foo.com, try to set
         //  cookie to domain ".com")
         //
         // If the cookie will not be set, it means ".com"
         // is a top level domain and we need to
         // set the cookie to ".foo.com"
         domainParts = host.split('.');
         domainParts.shift();
         domain = '.'+domainParts.join('.');

         document.cookie = name+"="+value+expires+"; path=/; domain="+domain;

         // check if cookie was successfuly set to the given domain
         // (otherwise it was a Top-Level Domain)
         if (Cookie.get(name) == null || Cookie.get(name) != value)
         {
            // append "." to current domain
            domain = '.'+host;
            document.cookie = name+"="+value+expires+"; path=/; domain="+domain;
         }
      }
   },

   get: function(name)
   {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i=0; i < ca.length; i++)
      {
         var c = ca[i];
         while (c.charAt(0)==' ')
         {
            c = c.substring(1,c.length);
         }

         if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
   },

   erase: function(name)
   {
      Cookie.set(name, '', -1);
   }
};
jQuery(document).ready(function(){
  var menu_4_mobile = new mobile_menu('.mobile-menu-wrapper','.mobile-menu-toggle', '.hide-mobile');
  menu_4_mobile.init();

  init_carousel();
})